# -*- encoding: utf8 -*-

import lxml.etree
import urllib, os, codecs, shutil, sys

# Совместимость с python3:
if(sys.version_info >= (3, 0)):
    import json, urllib.request as urllib
else:
    import simplejson as json, urllib

# парсер курсов валют с сайта cbr.ru
class CBRValuteParser:

    api_url = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req=' # 14/10/2014
    xml_tree = None

    # путь для сохранения кэша
    __cache_path = '/tmp/cbr_valute_parser_cache/'

    # сохраняет кэш
    def __save_cache(self, date, text):
        if not os.path.exists(self.__cache_path):
            os.makedirs(self.__cache_path)

        with codecs.open(self.__cache_path + date + '.cached.xml', 'wb') as f:
            f.write(text)
            f.close()

    # загружает из кэша
    def __load_cache(self, date):
        with codecs.open(self.__cache_path + date + '.cached.xml', 'rb') as f:
            text = f.read()
            f.close()
            return text

    # проверяет закеширован ли запрос
    def __check_cache(self, date):
        return os.path.isfile(self.__cache_path + date + '.cached.xml')

    # очищает кэш
    def clear_cache(self):
        if(os.path.isdir(self.__cache_path)):
            shutil.rmtree(self.__cache_path)

    # загружает xml с валютами
    def download_valute_list(self, date, no_cache = False):
        if self.__check_cache(date) and not no_cache:
            self.raw_xml = self.__load_cache(date)
        else:
            self.raw_xml = urllib.urlopen(self.api_url + date).read()
            
            if not no_cache:
                self.__save_cache(date, self.raw_xml)
        
        self.__create_tree()

    # выполняет построение DOM дерева
    def __create_tree(self):
        self.xml_tree = lxml.etree.XML(self.raw_xml)

    # ищет стоимость валюты
    def get_valute(self, charcode):
        xpath_str = "./Valute[CharCode[.='{}']]".format(charcode)
        
        element = self.xml_tree.xpath(xpath_str)
        
        if(element is not None):
            # value = filter(lambda el: el.tag == 'Value', element[0].getchildren())
            value = [ el for el in element[0].getchildren() if el.tag == 'Value' ]
            
            if len(value) > 0 and value[0] is not None:
                return value[0].text
            
            raise Exception("Value tag not found!")

        raise Exception("Valute tag not found!")