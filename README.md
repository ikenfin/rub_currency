# rub_currency

**rub_currency** - простой python скрипт для отображения курса валют относительно рубля.

Данные берутся из открытого [api](http://www.cbr.ru/scripts/Root.asp?PrtId=SXML) ЦБРФ.

Парсер данных ЦБРФ представлен классом CBRValuteParser и совместим с python 2 и 3 веток. Для работы парсера требуется модуль **lxml**.

Код доступен под лицензией [wtfpl](COPYING).

## Работа со скриптом:

`python rub_currency.py [options]`

Будучи вызванным без указания опций, скрипт загрузит текущий курс доллара:

    $ python rub_currency.py
    54,8219

Вызов справки:

    $ python rub_currency.py --help

Для просмотра доступных опций:

    $ python rub_currency.py --help
    
Указание валюты (множества валют через запятую):

    $ python rub_currency.py -veur
    62,2064
    $ python rub_currency.py -veur,usd
    62,2064
    54,8219

Указание даты по которой будет выводится курс:

    $ python rub_currency.py -d20.06.2015
    54,8219

*Внимание!*

Для ускорения работы скрипт кэширует загруженные xml файлы в директории `/tmp/cbr_valute_parser_cache`.

Чтобы запустить скрипт без использования кэша используется параметр **-f**

    $ python rub_currency.py -f
    
Для очистки кэша - параметр **--clear-cache**

    $ python rub_currency.py --clear-cache
    
## Решение проблем

При возникновении ошибки `lxml.etree.XMLSyntaxError: None` попробуйте очистить кэш **(--clear-cache)**.

## Дополнительно

В репозитории присутствует скрипт `rub_in.sh` для конвертации валют в рубли.

    sh rub_in.sh [COUNT] [CURRENCY_CODE]
        где:
            COUNT - количество, 
            CURRENCY_CODE - код валюты (eur)
    
*Пример*

    $ sh rub_in.sh 5
    Rubs in 5 usd: 272.6425
    $ sh rub_in.sh 10 EUR
    Rubs in 10 eur: 614.7540